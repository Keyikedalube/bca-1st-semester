/*
 * Program to see data type byte size
 */

#include <stdio.h>

int main()
{
    // character
    printf("char = %i\n", sizeof(char));
    puts("");

    // integer
    printf("int = %i\n", sizeof(int));
    printf("unsigned int = %i\n", sizeof(unsigned int));
    printf("long int = %i\n", sizeof(long int));
    printf("long long int = %i\n", sizeof(long long int));
    printf("unsigned long long int = %i\n", sizeof(unsigned long long int));
    puts("");

    // precision
    printf("float = %i\n", sizeof(float));
    printf("double = %i\n", sizeof(double));

    return(0);
}
