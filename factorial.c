#include <stdio.h>

int main()
{
	int number, product = 1;

	printf("Enter any number: ");
	scanf("%i", &number);

	while (number != 0)
		product *= number--;

	printf("The factorial is %i\n", product);

	return(0);
}
