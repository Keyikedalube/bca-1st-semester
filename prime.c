#include <stdio.h>

int main()
{
	int number, iterator, counter = 0;

	printf("Enter any number: ");
	scanf("%i", &number);

	for (iterator = 1; iterator <= number; iterator++)
		if (number%iterator == 0)
			++counter;
	if (counter == 2)
		printf("That number is prime\n");
	else
		printf("That number is not prime\n");

	return(0);
}
