#include <stdio.h>

#define SIZE 3

int main()
{
	int array[SIZE][SIZE];

	// initialize the array to zeros
	for (int row = 0; row < SIZE; row++)
		for (int col = 0; col < SIZE; col++)
			array[row][col] = 0;
	
	// print the array
	for (int row = 0; row < SIZE; row++) {
		for (int col = 0; col < SIZE; col++)
			printf("%i ", array[row][col]);
		printf("\n");
	}

	return(0);
}

