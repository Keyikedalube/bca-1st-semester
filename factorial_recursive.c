/*
 * Program to demonstrate recursive function on finding
 * the factorial of number
 */

#include <stdio.h>

int product = 1;
int factorial(int number);

int main()
{
	int number;

	printf("Enter any number: ");
	scanf("%i", &number);

	printf("The factorial is %i\n", factorial(number));

	return(0);
}

int factorial(int number)
{
	if (number != 0) {
		product *= number--;
		return factorial(number);
	}
	else
		return(product);
}
