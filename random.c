/*
 * Program to generate random numbers
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	srand(time(NULL));
	int no = rand();

	printf("no = %i\n", no);

	return(0);
}
