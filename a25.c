/*
 * Write a program to sort a given list of numbers.
 */

#include <stdio.h>
#include <stdlib.h>

#define SIZE 10

void print(int *array)
{
	for (int i = 0; i < SIZE; i++)
		printf("%i\t", array[i]);
	puts("");
}

int main()
{
	int array[SIZE];

	// store random numbers on the array
	for (int i = 0; i < SIZE; i++)
		array[i] = rand() % 10;

	puts("Before sorting:");
	print(array);

	// sort the array
	for (int i = 0; i < SIZE-1; i++) {
		for (int j = i+1; j < SIZE; j++) {
			if (array[i] > array[j]) {
				int temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
		}
	}

	puts("After sorting:");
	print(array);

	return(0);
}
