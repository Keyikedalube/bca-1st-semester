#include <stdio.h>

int main()
{
	int number;

	printf("Enter any number: ");
	scanf("%i", &number);

	if (number%2 == 0)
		printf("That number is even\n");
	else
		printf("That number is odd\n");

	return(0);
}
