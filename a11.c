/*
 * Write a program which will arrange the positive and negative numbers in a one-dimensional array
 * in such a way that all positive numbers should come first and then all the negative numbers will come
 * without changing original sequence of the numbers.
 * Example:
 * Original array contains: 10, -15, 1, 3, -2, 0, -2, -3, 2, -9
 * Modified array: 10, 1, 3, 0, 2, -15, -2, -2, -3, -9
 */

#include <stdio.h>

int array[] = {10, -15, 1, 3, -2, 0, -2, -3, 2, -9};
const int SIZE = sizeof(array) / sizeof(array[0]);

void print_array()
{
	for (short i = 0; i < SIZE; i++) {
		printf("%i", array[i]);
		if (i != SIZE-1)
			printf(", ");
		else
			puts("");
	}
}

int main()
{
	puts("Original array:");
	print_array();
	
	// Using bubble sorting algorithm to rearrange the array
	for (short i = 0; i < SIZE-1; i++) {
		for (short j = 0; j < SIZE-1; j++) {
			if (array[i] >= 0)
				continue;
			if (array[j] <= array[j+1]) {
				if (array[j] < 0 && array[j+1] >= 0) {
					//printf("Exchanging %i and %i\n", array[j], array[j+1]);
					int temp = array[j];
					array[j] = array[j+1];
					array[j+1] = temp;
				}
			}
		}
	}
	
	puts("Modified array:");
	print_array();
	
	return(0);
}
