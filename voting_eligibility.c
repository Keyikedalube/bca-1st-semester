#include <stdio.h>

int main()
{
	// get name from user
	printf("Enter your name: ");
	char name[20];
	scanf("%s", name);
	// get his or her age
	printf("Enter your age: ");
	int age;
	scanf("%i", &age);

	printf("Hello %s, ", name);
	if (age < 18)
		printf("sorry you are not eligible to vote\n");
	else
		printf("you are eligible to vote\n");
	
	return(0);
}

