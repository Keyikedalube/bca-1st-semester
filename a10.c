/*
 * Apply recursive call to do the following:
 * i)	Input 'n' (1-200). Calculate sum of 'n' numbers
 * ii)	Input 'n' (1-20). Calculate product of 'n' numbers
 * iii)	Input 'n' (2-20). Print 'n' number of Fibonacci numbers. In Fibonacci sequence
 *      the sum of two successive terms gives the third term.
 */

#include <stdio.h>
#include <stdlib.h>

int recursive_sum = 0;
int recursive_product = 1;

// Fibonacci variables
int recursive_fibonacci = 0;
int first_term = 1;
int second_term = 2;

int sum(int n)
{
	if (n == 0)
		return(recursive_sum);
	else {
		recursive_sum += n--;
		// do not try to decrement while recursively calling its function
		// at the same time because it doesn't work
		return(sum(n));
	}
}

int product(int n)
{
	if (n != 0) {
		recursive_product *= n--;
		// do not try to decrement while recursively calling its function
		// at the same time because it doesn't work
		return(product(n));
	}
	else
		return(recursive_product);
}

int fibonacci(int n)
{
	if (n-- != 0) {
		recursive_fibonacci = first_term + second_term;
		first_term = second_term;
		second_term = recursive_fibonacci;
		// do not try to decrement while recursively calling its function
		// at the same time because it doesn't work
		return(fibonacci(n));
	}
	else
		return(recursive_fibonacci);
}

int main()
{
	// recursive sum
	printf("Enter integer value of n (1-200): ");
	int n = 0;
	scanf("%i", &n);
	if (n <= 0 || n >= 201)
		puts("Input is out of range!");
	else
		printf("The recursive sum is: %i\n", sum(n));

	// recursive product
	printf("Enter integer value of n (1-20): ");
	scanf("%i", &n);
	if (n <= 0 || n >= 21)
		puts("Input is out of range!");
	else
		printf("The recursive product is: %i\n", product(n));

	// recursive Fibonacci
	printf("Enter integer value of n (2-20): ");
	scanf("%i", &n);
	if (n <= 1 || n >= 21)
		puts("Input is out of range!");
	else {
		printf("The recursive Fibonacci number is: %i\n", fibonacci(n));
	}

	return(0);
}
