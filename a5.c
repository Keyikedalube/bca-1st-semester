/*
 * Given an integer number, write a program that displays the numbers
 * as follows:
 * First Line	:	All digits
 * Second Line	:	All except first digit
 * Third line	:	All except first two digits
 * ...
 * Last line	:	The last digit
 *
 * For example, the number 5678 will be displayed as:
 * 5678
 * 678
 * 78
 * 8
 */

#include <stdio.h>

int main()
{
	int number[] = {1, 2, 3, 4, 5, 6, 7};
	// count to n terms of the array to eliminate the initial index progressively
	short unsigned counter = 0;

	// compute array size
	short unsigned size = sizeof(number) / sizeof(number[0]);

	while (counter < size) {
		for (short unsigned index = counter; index < size; index++)
			printf("%i", number[index]);
		printf("\n");
		counter++;
	}

	return(0);
}
