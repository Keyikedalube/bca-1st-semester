/*
 * Write a program to find the sum of all prime numbers between 100 and 500
 */

#include <stdio.h>

int main()
{
	// important to initialize (default value) every variable that is declared
	unsigned dividend = 100, divider = 0;
	unsigned remainder_counter = 0;
	unsigned sum_prime = 0;

	printf("\tSum of all prime numbers between 100 and 500:\n");

	for (; dividend <= 500; dividend++) {
		// it's a must to reset counter to 0 for every dividend
		remainder_counter = 0;
		// A number divisible by 1 and itself is a prime number
		// dividend will be divided by divider till <= dividend itself
		for (divider = 1; divider <= dividend; divider++)
			if (dividend % divider == 0)
				remainder_counter++;
		if (remainder_counter == 2)
			sum_prime += dividend;
	}

	printf("\t\t\t\t%u\n", sum_prime);

	return(0);
}

