/*
 * Define 2 dimensional arrays a(3, 3),  b(3, 3),  sum(3, 3),
 * diff(3, 3), mult(3, 3). Store 9 arbitrary numbers in a(3, 3)
 * and b(3, 3).
 * Do the following:
 *
 * Calculate sum of a(3, 3) and b(3, 3) and store in sum(3, 3)
 * where  sum(i, j) = a(i, j) + b(i, j)
 *
 * Calculate difference of a(3, 3) and b(3, 3) and store in
 * diff(3, 3) where diff(i, j) = a(i, j) - b(i, j)
 *
 * Calculate product of two arrays a(3, 3) and b(3, 3) and store
 * in mult(3, 3) where mult(i, j) = summation of a(i, k) *
 * b(k, j) over k where k = 1 to 3.
 *
 * Print the result in a tabular form
 */

#include <stdio.h>
#include <stdlib.h>

const int SIZE = 3;		// 2D array size
unsigned short row = 0;
unsigned short col = 0;

void separator()
{
	for (row = 0; row < 40; row++)
	printf("-");
}

void print(int matrix[SIZE][SIZE], char *string)
{
	separator();
	printf("\n%s\n", string);
	for (int row = 0; row < SIZE; row++) {
		printf("\t");
		for (int col = 0; col < SIZE; col++)
			printf("%i\t", matrix[row][col]);
		puts("");
	}
	puts("");
}

int main()
{
	int a[SIZE][SIZE];
	int b[SIZE][SIZE];
	int sum[SIZE][SIZE];		// store computed sum
	int diff[SIZE][SIZE];		// store computed difference
	int mult[SIZE][SIZE];		// store computed multiplication

	// store 9 arbitrary numbers in 'a'
	for (row = 0; row < SIZE; row++)
		for (col = 0; col < SIZE; col++)
			a[row][col] = rand() % 10;
	// store 9 arbitrary numbers in 'b'
	for (row = 0; row < SIZE; row++)
		for (col = 0; col < SIZE; col++)
			b[row][col] = rand() % 10;

	// print a
	print(a, "Displaying contents of a(3, 3)");
	// print b
	print(b, "Displaying contents of b(3, 3)");

	// compute sum
	for (row = 0; row < SIZE; row++)
		for (col = 0; col < SIZE; col++)
			sum[row][col] = a[row][col] + b[row][col];

	// print computed sum
	print(sum, "Sum of a(3, 3) & b(3, 3)");

	// compute difference
	for (row = 0; row < SIZE; row++)
		for (col = 0; col < SIZE; col++)
			diff[row][col] = a[row][col] - b[row][col];

	// print computed difference
	print(diff, "Difference of a(3, 3) & b(3, 3)");

	// print multiplication process for testing purposes
/*	puts("Multiplication process");*/
/*	for (row = 0; row < SIZE; row++) {*/
/*		for (col = 0; col < SIZE; col++) {*/
/*			for (int k = 0; k < SIZE; k++) {*/
/*				printf("%i*%i", a[row][k], b[k][col]);*/
/*				if (k < SIZE-1)*/
/*					printf("+");*/
/*			}*/
/*			printf(" ");*/
/*		}*/
/*		printf("\n");*/
/*	}*/
/*	puts("");*/

	// compute multiplication
	for (row = 0; row < SIZE; row++)
		for (col = 0; col < SIZE; col++) {
			int sum = 0;
			for (int k = 0; k < SIZE; k++)
				sum += a[row][k] * b[k][col];
			mult[row][col] = sum;
		}

	// print computed multiplication
	print(mult, "Product of a(3, 3) & b(3, 3)");

	return(0);
}
