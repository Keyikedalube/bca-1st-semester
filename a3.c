/*
 * Write a program to reverse the digits of a given number.
 * For example, the number 9876 should be returned as 6789.
 */

#include <stdio.h>

int main()
{
	printf("Enter the number: ");
	int number = 0;
	scanf("%i", &number);

	printf("The reverse is: ");
	while (number > 0) {
		printf("%i", number % 10);
		number /= 10;
	}
	puts("");

	return(0);
}
