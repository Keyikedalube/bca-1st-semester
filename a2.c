/*
 * Write a program to obtain sum of the first 10 terms of the following
 * series for any positive integer value of x:
 *
 * 			x + x^3/3! + x^5/5! + x^7/7! + ... + x^10/10!
 */

#include <stdio.h>
// compile by linking to math library using -lm on terminal emulator program
#include <math.h>

int main()
{
	printf("Enter value of x: ");
	// positive integer value of x
	unsigned x = 0;
	scanf("%u", &x);
	unsigned long sum_whole = x;		// whole numbers
	double sum_precision = x;		// with precision
	unsigned factorial = 1;

	for (short unsigned power = 3; power <= 10; power += 2) {
		// last term is sum of 3 and not 2
		if (power == 9)
			power++;
		// compute the factorial
		for (short unsigned constant = power; constant > 0; constant--)
			factorial *= constant;
		// compute an individual operand set
		sum_whole += pow(x, power) / factorial;
		sum_precision += pow(x, power) / factorial;
	}

	// print the sum of the 10 terms
	printf("The (whole) sum is: %lu\n", sum_whole);
	printf("The (precision) sum is: %f\n", sum_precision);

	return(0);
}
