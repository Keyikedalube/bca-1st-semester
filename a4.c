/*
 * Write an interactive program to demonstrate the process of
 * multiplication.
 * The program should ask the user to enter two-digit integers and print
 * the product of integer as shown below.
 *						    45
 *					x	    37
 *	 					------
 *				7 x 45 is	   315
 *				3 x 45 is	  135
 *						------
 *			Adding them we get	  1665
 */

#include <stdio.h>
#include <stdlib.h>

int two_digit_checker(short digit)
{
	short counter = 0;
	while (digit != 0) {
		digit /= 10;
		counter++;
	}
	if (counter == 2)
		return 0;
	else {
		puts("Invalid input! The program specifically asked for two digit...");
		return 1;
	}
}

int main()
{
	printf("Enter value of x (two-digit only): ");
	short x = 0;
	scanf("%hi", &x);
	// check if x is two digit or not
	if (two_digit_checker(x) != 0)
		exit(1);
	// check if y is two digit or not
	printf("Enter value of y (two-digit only): ");
	short y = 0;
	scanf("%hi", &y);
	if (two_digit_checker(y) == 1)
		exit(1);
	int product = x * y;

	printf("\n\t\t\t%hi\n", x);
	printf("\t\t    x   %hi\n", y);
	printf("\t\t    ----------\n");
	// split y value to multiply x individually
	printf("\t%hi x %hi is %8i\n", y % 10, x, y % 10 * x);
	y /= 10;
	printf("\t%hi x %hi is %7i\n", y % 10, x, y % 10 * x);
	printf("\t\t    ----------\n");
	printf("Adding them we get%8i\n", product);

	return(0);
}
