/*
 * Write a program to traverse a singly link list in reverse order and write out the contents too.
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct datum {
    int number;
    struct datum *next;
} node;

node *head = NULL;
node *link = NULL;
node *temporary = NULL;

int i = 0;	// iterator
int size = 0;	// list size

void input_datum()
{
	if (temporary == NULL)
		printf("\tAllocation failed :(");
	else {
		printf("\tEnter integer data %i: ", i);
		scanf("%d", &(*temporary).number);
	}
}

void print()
{
	if (head == NULL)
		printf("\tNo list to print :(\n");
	else {
		link = head;
		printf("\t");
		while (link != NULL) {
			printf("%d->", (*link).number);
			link = (*link).next;
		}
		printf("NULL\n");
	}
}

void reverse()
{
	link = head;
	temporary = NULL;
	
	int loop = 1;
	while (loop) {
		head = (*head).next;
		(*link).next = temporary;
		temporary = link;
		if (head == NULL) {
			head = link;
			loop = 0;
		} else
			link = head;
	}
}

int main()
{
	printf("\tEnter the size of nodes: ");
	scanf("%i", &size);
	
	if (size != 0) {
		temporary = malloc(sizeof(node));
		head = temporary;
		// node 1 is already created before for loop executes
		// So the allocation during the loop would be size-1
		for (i = 1, link = head; i <= size; i++) {

			input_datum();

			if (i == size)
				break;
				
			temporary = malloc(sizeof(node));
			(*link).next = temporary;
			link = temporary;
		}
		(*link).next = NULL;
	}

	puts("\n\tBefore reversing");
	print();

	reverse();

	puts("\n\tAfter reversing");
	print();

	// delete the list
	while (head != NULL) {
		temporary = head;
		head = (*head).next;
		
		free(temporary);
	}

	return(0);
}
