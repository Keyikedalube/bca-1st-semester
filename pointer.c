#include <stdio.h>

int number;
int *pointer = &number;

void print_number()
{
	printf("\nnumber value = %i", number);
	printf("\nnumber address = %p", &number);
}

void print_pointer()
{
	printf("\npointer value = %i", *pointer);
	printf("\npointer address = %p", pointer);
}

int main()
{
	printf("Enter any number: ");
	scanf("%i", &number);

	print_number();
	print_pointer();

	number = number + 3;
	print_number();
	print_pointer();

	*pointer = *pointer * 3;
	print_number();
	print_pointer();

	printf("\n");

	return(0);
}
