/*
 * Write a program to replace ‘a’ with ‘b’, ‘b’ with ‘c’, ..., ’z’ with ‘a’
 * and similarly for ‘A’ with ‘B’, ’B’ with ‘C’, ..., ‘Z’ with ‘A’ in a file.
 * The other characters should remain unchanged.
 */

#include <stdio.h>

#define SIZE 1000

int main()
{
	FILE *file;
	file = fopen("a26.txt", "w+");
	if (file == NULL) {
		puts("Something went wrong opening the \"file.txt\" :(");
		return(1);
	}
	char string[SIZE];
	
	//for (short i = 0; i < 127; i++)
	//	printf("%i\t=\t%c\n", i, i);
	
	puts("Write out something rubbish:");
	scanf("%[^\n]s", string);
	
	for (int i = 0; string[i] != '\0'; i++) {
		short ascii = string[i];
		if (string[i] >= 65 && string[i] <= 90) {
			// Replace capital letters
			if (string[i] == 90)
				string[i] = 65;
			else
				string[i] = ++ascii;
		} else if (string[i] >= 97 && string[i] <= 122) {
			// Replace small letters
			if (string[i] == 122)
				string[i] = 97;
			else
				string[i] = ++ascii;
		}
		fprintf(file, "%c", string[i]);
	}
	
	printf("Converted string written to a file successfully!\n");
	fclose(file);
	
	return(0);
}
