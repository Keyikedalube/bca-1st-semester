/*
 * Program to swap two numbers using
 * 	Call by value
 * 	Call by reference
 */

#include <stdio.h>

void value(int x, int y);
void reference(int *x, int *y);
void print(int x, int y);

int main()
{
	int x, y;

	printf("Enter the value of x: ");
	scanf("%i", &x);
	printf("Enter the value of y: ");
	scanf("%i", &y);

	puts("\nSwap by value:");
	value(x, y);

	// print original value once more
	puts("\nCurrent value:");
	print(x, y);

	puts("\nSwap by reference:");
	reference(&x, &y);
	print(x, y);

	// print original value once more
	puts("\nCurrent value:");
	print(x, y);

	return(0);
}

void value(int x, int y)
{
	int temporary = x;
	x = y;
	y = temporary;
	print(x, y);
}

void reference(int *x, int *y)
{
	int temporary = *x;
	*x = *y;
	*y = temporary;
}

void print(int x, int y)
{
	printf("x = %i\n", x);
	printf("y = %i\n", y);
}
