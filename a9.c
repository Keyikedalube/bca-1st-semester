/*
 * Write a C function to remove duplicates from an ordered array.
 * For example, if input array contains 10, 10, 10, 30, 40, 40, 50, 80, 80, 100
 * then output should be 10, 30, 40, 50, 80, 100.
 */

#include <stdio.h>
#include <stdlib.h>

void bubble_sort(int array[], int size)
{
	int flag = 0;
	for (unsigned int i = 0; i < size && !flag; i++) {
		flag = 1;
		for (unsigned j = 0; j < size-1; j++) {
			if (array[j] > array[j+1]) {
				int temp = array[j];
				array[j] = array[j+1];
				array[j+1] = temp;
				flag = 0;
			}
		}
	}
}

void print_array(int array[], int size)
{
	for (unsigned int i = 0; i < size; i++)
		printf("%u\t", array[i]);
	printf("\n");
}

int main()
{
	const int SIZE = 10;
	int array[SIZE];

	// Set random values
	for (unsigned int i = 0; i < SIZE; i++)
		array[i] = rand() % 10;

	bubble_sort(array, SIZE);
	print_array(array, SIZE);

	// Remove duplicates
	int new_size = SIZE - 1;
	for (unsigned int i = 0; i < SIZE-1; i++) {
		for (unsigned loop_i = i+1; loop_i < new_size; loop_i++) {
			if (array[i] == array[loop_i]) {
				// replace duplicate with the last index value
				array[loop_i] = array[new_size];
				array[new_size] = 0;
				new_size--;
			}
		}
	}

	bubble_sort(array, new_size);
	print_array(array, new_size);

	return(0);
}
