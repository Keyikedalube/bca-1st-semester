/*
 * Program to get string (with spaces) from user
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
	char string[100];

	printf("Enter the string: ");
	scanf("%[^\n]s", string);

	printf("The string is: %s\n", string);

	return(0);
}
