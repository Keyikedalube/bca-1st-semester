/*
 * Write a program to input 20 arbitrary numbers in one-dimensional
 * array. Calculate frequency of each number. Print the number and its
 * frequency in a tabular form.
 */

#include <stdio.h>
#include <stdlib.h>

#define SIZE 20

void print_array(int array[])
{
	puts("The array elements are:");
	for (int index = 0; index < SIZE; index++)
		printf("%i ", array[index]);
	puts("");
}

void print_frequency(int index_value, unsigned short frequency)
{
	printf("%i\t=\t%u ", index_value, frequency);
	if (frequency == 1)
		printf("time\n");
	else
		printf("times\n");
}

int main()
{
	int array[SIZE];
	int registered_element[SIZE];
	unsigned short index1 = 0;		// for array
	unsigned short index2 = 0;		// for frequency array
	unsigned short frequency = 0;

	// get all the inputs
	for (index1 = 0; index1 < SIZE; index1++) {
		do {
			printf("Enter value of array[%u]:\t", index1);
			scanf("%i", &array[index1]);
			if (array[index1] == 0)
				/* 0 is exempted in this program because some elements in registered_element array
				 * is initialized (by default) with the same value and gets skipped, therefore,
				 * frequency count for value 0 is never computed at all
				 */
				puts("...Please enter a value other than 0");
			else
				break;
		} while (1);
	}

	print_array(array);

	// get the frequency
	index1 = 0;
	puts("\nThe frequency table is:");
	while (index1 < SIZE) {
		// reset for next iteration
		frequency = 0;
		for (index2 = 0; index2 < SIZE; index2++) {
			if (array[index1] == registered_element[index2]) {
				// same value was computed for frequency, so jump away
				goto registered;
			} else if (array[index1] == array[index2]) {
				frequency++;
			}
		}
		print_frequency(array[index1], frequency);
		// register the new value
		registered_element[index1] = array[index1];
		registered:
			index1++;
	}

	//print_array(registered_element);

	return(0);
}
