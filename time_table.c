/*
 * Time table from 1 to 10
 */

#include <stdio.h>

#define TIMES 10

int main()
{
	int number = 0;
	int iterator = 0;

	for (number = 1; number <= TIMES; number++) {
		for (iterator = 1; iterator <= TIMES; iterator++)
			printf("%2i x %2i = %3i\n", number, iterator, number * iterator);
		puts("");
	}

	return(0);
}

