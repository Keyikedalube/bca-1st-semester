/*
 * Program to demonstrate constant value
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	time_t t;
	srand(time(&t));
	const int value = random();

	printf("value = %i\n", value);

	return(0);
}
