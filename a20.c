/*
 * Write a program to convert a given decimal number to its binary equivalent and vice versa.
 */

#include <stdio.h>
#include <math.h>

void decimal_to_binary()
{
    // get decimal
    printf("Enter decimal number: ");
    int decimal = 0;
    scanf("%i", &decimal);

    // convert to binary
    int binary[1000];
    int iterator = 0;
    while (decimal != 0) {
        if (decimal % 2 != 0) {
            binary[iterator++] = 1;
        } else {
            binary[iterator++] = 0;
        }
        decimal /= 2;
    }

    // print the binary output
    printf("Binary equivalent is: ");
    do {
        printf("%i", binary[--iterator]);
    } while (iterator != 0);
}

void binary_to_decimal()
{
input_again:
    // get binary
    printf("Enter binary number: ");
    long long int binary = 0;
    scanf("%lli", &binary);

    // convert to decimal
    int decimal = 0;
    int power = 0;
    short bit = 0;
    while (binary != 0) {
        bit = binary % 10;
        if (bit == 0 || bit == 1) {
            decimal += bit * pow(2, power++);
        } else {
            puts("Invalid binary number! Try another one.");
            goto input_again;
        }
        binary /= 10;
    }

    // print the decimal output
    printf("Decimal equivalent is: %i\n", decimal);
}

int main()
{
    // loop driven program
    do {
        puts("");
        puts("1) Decimal to Binary conversion.");
        puts("2) Binary to Decimal conversion.");
        puts("3) Exit");
        printf("Enter your option: ");
        int option = 0;
        scanf("%i", &option);
        if (option == 1) {
            decimal_to_binary();
        } else if (option == 2) {
            binary_to_decimal();
        } else if (option == 3) {
            break;
        } else {
            puts("Invalid option! Try again.");
        }
    } while (1);

    return(0);
}
