/*
 * Write a menu driven program to create a linked list and perform the following operations
 *   a) Insert an item
 *      i. At the front of the list
 *      ii. End of the list
 *      iii. Before a specified Node
 *   b) Delete an item
 *      i. The first Node
 *      ii. Last Node
 *      iii. A specified Node
 *   c) Search for an Item in the list
 *   d) Display the List
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct singly_linked_list {
	int number;
	struct singly_linked_list *next;
} list;

list *node;
list *head;
list *current;

int no_of_nodes = 0;	// list length

void input()
{
	node = malloc(sizeof(node));
	puts("");
	printf("\t\tEnter a number: ");
	scanf("%i", &node->number);
	puts("");
	
	no_of_nodes++;
}

void new_front_node()
{
	input();
	
	if (head == NULL) {
		head = node;
		head->next = NULL;
	} else {
		node->next = head;
		head = node;
	}
}

void new_end_node()
{
	current = head;
	while (current->next != NULL)
		current = current->next;
		
	input();
	
	current->next = node;
	node->next = NULL;
}

void delete_front_node()
{
	if (head != NULL) {
		current = head->next;
		free(head);
		head = current;
		
		puts("");
		puts("\t\tFront node deleted.");
		puts("");
		
		no_of_nodes--;
	} else
		puts("\n\t\tEMPTY LIST!\n");
}

void delete_random_node(int position)
{
	if (head != NULL) {
		current = head;
		list *temporary = head;
		
		for (unsigned i = 1; i <= no_of_nodes; i++) {
			if (i == position) {
				temporary->next = current->next;
				free(current);
				
				puts("");
				puts("\t\tNode deleted.");
				puts("");
				
				no_of_nodes--;
			} else {
				temporary = current;
				current = current->next;
			}
		}
	} else
		puts("\n\t\tEMPTY LIST!\n");
}

int search(int value)
{
	int found = 0;
	
	if (head != NULL) {
		current = head;
		while (current != NULL) {
			if (value == current->number) {
				found = 1;
				break;
			}
			current = current->next;
		}
	}
	return(found);
}

int main()
{
	char choice;	// menu driven choice
	
	do {
		printf("a) Insert an item\n");
		printf("b) Delete an item\n");
		printf("c) Search an item\n");
		printf("d) Display the list\n");
		printf("e) Exit\n");
		printf("\tEnter your choice: ");
		scanf(" %c", &choice);
		
		switch (choice) {
			int option;	// sub menu driven option
			int position;	// node position

			case 'a':
				// Insert an item
				printf("\t1) Front\n");
				printf("\t2) End\n");
				printf("\t3) Specify position\n");
				printf("\t\tEnter your choice: ");
				scanf("%i", &option);
				
				switch (option) {
					case 1:
						new_front_node();
						break;
					case 2:
						new_end_node();
						break;
					case 3:
						// Random position on the list
						do {
							printf("\t\tEnter position (1-%i): ", no_of_nodes+1);
							scanf("%i", &position);
						} while (position < 1 || position > no_of_nodes+1);
						
						current = head;
						if (position == 1) {
							new_front_node();
							break;
						} else if (position == no_of_nodes+1) {
							new_end_node();
							break;
						}
						for (unsigned i = 1; i <= position; i++) {
							if (i == position-1) {
								input();
								node->next = current->next;
								current->next = node;
							} else
								current = current->next;
						}
						break;
					default:
						puts("\nINVALID CHOICE!\n");
				}
				break;
			case 'b':
				// Delete an item
				printf("\t1) Front\n");
				printf("\t2) End\n");
				printf("\t3) Specify position\n");
				printf("\t\tEnter your choice: ");
				scanf("%i", &option);
				
				switch (option) {
					case 1:
						delete_front_node();
						break;
					case 2:
						if (no_of_nodes == 1)
							delete_front_node();
						else
							delete_random_node(no_of_nodes);
						break;
					case 3:
						do {
							printf("\t\tEnter position (1-%i): ", no_of_nodes);
							scanf("%i", &position);
						} while (position < 1 || position > no_of_nodes);
						
						if (position == 1)
							delete_front_node();
						else
							delete_random_node(position);
						break;
					default:
						puts("\nINVALID CHOICE!\n");
				}
				break;
			case 'c':
				// Search an item
				printf("\n\tEnter value: ");
				int value;
				scanf("%i", &value);
				
				puts("");
				if (search(value)) {
					printf("\tValue exist in the list.");
				} else {
					printf("\tValue doesn't exist in the list.");
				}
				puts("\n");
				break;
			case 'd':
				// Display the list
				puts("");
				current = head;
				while (current != NULL) {
					printf("%i->", current->number);
					current = current->next;
				}
				puts("NULL\n");
				break;
			case 'e':
				// Exit
				break;
			default:
				puts("\nINVALID CHOICE!\n");
		}
	} while (choice != 'e');
	
	// clear the list
	while (head != NULL) {
		current = head;
		head = head->next;
		free(current);
	}
	
	return(0);
}
